<?php

use App\Controllers\HomeController;
use Src\Http\Route;

// route for home page
Route::get('/', [HomeController::class, 'index']);

// route for log file
Route::post('/get-file', 'LogController@getFile');
Route::get('/paginate-file', 'LogController@paginatePage');

//route for auth
Route::get('/login', 'Auth\LoginController@loginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LogoutController@logout');
