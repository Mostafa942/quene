<?php

session_start();

if (!empty($_SESSION['user'])) {
    header('Location: ');
    exit();
} else {
    header('Location: /');
    exit();
}
