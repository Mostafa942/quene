<?php

namespace App\Controllers;

class LogController
{
    /**
     * get log file from path
     * @return json file content
     */
    public function getFile()
    {
        $data = request()->only('search');
        $validation = $this->validation($data);
        if (count($validation)) {
            http_response_code(422);
            header('Content-type: application/json');
            echo json_encode($validation);
            die();
        }
        $filePath = base_path() . 'logs/' . $data['search'] . '.log';
        $file = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $output = $this->getFromFile($file);
        $output['file_path'] = $data['search'];
        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode($output);
    }

    /**
     * paginate between pages of file
     * @return json file content
     */
    public function paginatePage()
    {
        $parameter = request()->parameter();
        $filePath = base_path() . 'logs/' . $parameter['path'] . '.log';
        $file = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $output = $this->getFromFile($file, $parameter['page']);
        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode($output);
    }

    /**
     * read from file and return content from file
     * @param array $file
     * @param array $page = 0
     * @return array $response
     */
    public function getFromFile($file, $page = 0)
    {
        $linesPerPage = 10;

        $numOfPages = ceil(count($file) / $linesPerPage) - 1;

        if ($page >= 1) {
            array_splice($file, 0, ($page * $linesPerPage));
        }

        $response = [];

        $response['output'] = array_slice($file, 0, $linesPerPage);

        $response['num_of_pages'] = $numOfPages;

        $response['current_page'] = $page;

        $response['next_page'] = $page + 1;

        $response['previous_page'] = $page - 1;

        return $response;
    }

    /**
     * make validation to file name with return validation error
     * @param array $data
     * @return array $errors
     */
    protected function validation($data)
    {
        $errors = [];
        if (empty($data['search'])) {
            $errors['search'] = 'please type path of file';
        } elseif (!is_string($data['search'])) {
            $errors['search'] = 'invalid data';
        } elseif (!file_exists(base_path() . 'logs/' . $data['search'] . '.log')) {
            $errors['search'] = 'file not exists';
        }
        return $errors;
    }
}
