<?php

namespace App\Controllers\Auth;

use GrahamCampbell\ResultType\Success;
use Src\Http\Request;
use Src\View\View;

class LoginController
{
    /**
     * construct to start session and check if auth
     * @return void
     */
    public function __construct()
    {
        session_start();
        if (!empty($_SESSION['user'])) {
            header('Location: /');
            exit();
        }
    }

    /**
     * get login form 
     * @return view login
     */
    public function loginForm()
    {
        return view('auth.login');
    }

    /**
     * make auth
     * @return json view home
     */
    public function login()
    {
        $data = request()->only(['username', 'password']);
        $validation = $this->validate($data);
        if (count($validation)) {
            http_response_code(422);
            header('Content-type: application/json');
            echo json_encode($validation);
            die();
        }
        $_SESSION['user'] = 'admin';
        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode(View::toHtml('home'));
    }

    /**
     * make validation to username and password with return validation error
     * @param array $data
     * @return array $errors
     */
    protected function validate($data)
    {
        $errors = [];
        //validation for Username
        if (!isset($data['username']) || empty($data['username'])) {
            $errors['username'] = 'username is required';
        } elseif ($data['username'] != 'admin') {
            $errors['username'] = 'invalid username';
        }

        //validation for Password
        if (!isset($data['password']) || empty($data['password'])) {
            $errors['password'] = 'password is required';
        } elseif ($data['password'] != 'admin') {
            $errors['password'] = 'invalid password';
        }

        return $errors;
    }
}
