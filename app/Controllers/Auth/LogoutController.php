<?php

namespace App\Controllers\Auth;

class LogoutController
{
    public function __construct()
    {
        session_start();
        if (empty($_SESSION['user'])) {
            header('Location: /login');
            exit();
        }
    }

    public function logout()
    {
        unset($_SESSION['user']);
        header('Location: /login');
        exit();
    }
}
