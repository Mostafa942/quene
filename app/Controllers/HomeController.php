<?php

namespace App\Controllers;

class HomeController
{
    /**
     * construct to start session and check if auth
     * @return void
     */
    public function __construct()
    {
        session_start();
        if (empty($_SESSION['user'])) {
            header('Location: /login');
            exit();
        }
    }

    /**
     * get home page
     * @return view home
     */
    public function index()
    {
        return view('home');
    }
}