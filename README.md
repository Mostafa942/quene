## Log File Viewer

This application reads in a log file path and the file content is paginated 10 lines at a time.

## Application  Setup

### Prerequisites
You have to have docker installed

### Initialization Steps (Enter the following commands in the command prompt)
1) Install the required docker containers

    `docker-compose up -d php-fpm workspace`

2) Use zsh in the workspace conainer

    `docker-compose exec --user=laradock workspace zsh`

3) Navigate into the project directory

    `cd app`

4) Run composer install to install the required packages

    `composer install`

### How to use the application
1) Login with the username "admin" and the password "admin"
2) Enter the log file path in the text field.
3) Click on the navigation buttons below the file lines display table to navigate the whole file
