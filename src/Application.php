<?php

namespace Src;

use Src\Http\Route;
use Src\Http\Request;
use Src\Http\Response;
use Src\Support\Session;

class Application
{
    protected Request $request;

    protected Response $response;

    protected Route $route;

    protected Session $session;

    public function __construct()
    {
        $this->request = new Request();

        $this->response = new Response();

        $this->route = new Route($this->request, $this->response);

        $this->session = new Session();
    }

    public function run()
    {
        $this->route->resolve();
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
    }
}
