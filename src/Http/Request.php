<?php

namespace Src\Http;

use Src\Support\Arr;

class Request
{
    protected $path;


    /**
     * define path
     * @return void;
     */
    public function __construct()
    {
        $this->path = strtolower($_SERVER['REQUEST_URI']) ?? '/';
    }
    /**
     * get method of request
     * @return string $methodType
     */
    public function getMethod(): string
    {
        $methodType = strtolower($_SERVER['REQUEST_METHOD']);
        return $methodType;
    }

    /**
     * path to file
     * @return string $path
     */
    public function path(): string
    {
        $path = str_contains($this->path, '?') ? explode('?', $this->path)[0] : $this->path;
        return $path;
    }

    /**
     * get query query parameters
     * @return array $queryParameters
     */
    public function parameter(): array
    {
        $queryParameters = str_contains($this->path, '?') ? explode('?', $this->path)[1] : [];
        if (is_string($queryParameters)) {
            parse_str($queryParameters, $parameters);
            $queryParameters = $parameters;
        }
        return $queryParameters;
    }

    /**
     * get all items of request
     * @return array $_REQUEST
     */
    public function all()
    {
        return $_REQUEST;
    }

    /**
     * get only items of request by keys
     * @return array $items
     */
    public function only($keys)
    {
        $items = Arr::only($this->all(), $keys);
        return $items;
    }

    /**
     * get only one item from request by its key
     * @return array $item
     */
    public function get($key)
    {
        $item = Arr::get($this->all(), $key);
        return $item;
    }
}
