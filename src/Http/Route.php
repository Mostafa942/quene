<?php

namespace Src\Http;

use Src\View\View;

class Route
{
    protected static $routes = [];

    public $request;

    public $response;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * get method for get requests
     * @param string $route
     * @param  callable|array|string $action
     * @return void
     */
    public static function get(string $route, callable|array|string $action): void
    {
        self::$routes['get'][$route] = $action;
    }

    /**
     * get method for post requests
     * @param string $route
     * @param  callable|array|string $action
     * @return void
     */
    public static function post(string $route, callable|array|string $action): void
    {
        self::$routes['post'][$route] = $action;
    }

    /**
     * resolve action
     * @return void
     */
    public function resolve(): void
    {
        $path = $this->request->path();
        $method = $this->request->getMethod();
        $parameters = $this->request->parameter();
        $action = self::$routes[$method][$path] ?? false;
        if (!$action) {
            View::makeError('404');
        }

        if (is_callable($action)) {
            call_user_func_array($action, array_values($parameters));
        }

        if (is_array($action)) {
            call_user_func_array([new $action[0], $action[1]], array_values($parameters));
        }

        if (is_string($action)) {
            $action = explode('@', $action);
            $controller = 'App\Controllers\\' . $action[0];
            call_user_func_array([new $controller, $action[1]], array_values($parameters));
        }
    }
}
