<?php

use Src\Application;
use Src\Http\Request;
use Src\View\View;

if (!function_exists('app')) {
    function app()
    {
        static $instance = null;

        if (!$instance) {
            $instance = new Application();
        }

        return $instance;
    }
}

if (!function_exists('dd')) {
    function dd($debug)
    {
        dump($debug);
        die();
    }
}

if (!function_exists('env')) {
    function env($key, $default = 'Quene')
    {
        return $_ENV[$key] ?? $default;
    }
}

if (!function_exists('base_path')) {
    function base_path()
    {
        return dirname(__DIR__) . '/../';
    }
}

if (!function_exists('view_path')) {
    function view_path()
    {
        return base_path() . 'views/';
    }
}



if (!function_exists('view')) {
    function view($view, $params = [])
    {
        return View::make($view, $params);
    }
}

if (!function_exists('value')) {
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (!function_exists('request')) {
    function request()
    {
        return new Request();
    }
}

if (!function_exists('old')) {
    function old($key)
    {
        if (app()->session->hasFlas('old')) {
            return app()->session->getFlash('old')[$key];
        }
    }
}

if (!function_exists('auth_user')) {
    function auth_user()
    {
        return !empty($_SESSION['user']);
    }
}
