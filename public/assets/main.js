//login function
$(document).on('submit', '#loginForm', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/login',
        dataType: 'json',
        data: $('#loginForm').serialize(),
        success: function (data) {
            $('#main-content').html(data);
            $('#logout').removeClass('d-none');
            var currentUrl = window.location.href;
            var newUrl = currentUrl.replace('login', '');
            const nextState = {
                additionalInformation: 'Updated the URL with JS'
            };
            window.history.pushState(nextState, 'Quene', newUrl);
        },
        error: function (data) {
            let response = data.responseJSON;
            validation(response);
        }
    })
})

// submit search for file
$(document).on('submit', '#search-form', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/get-file',
        dataType: 'json',
        data: $('#search-form').serialize(),
        success: function (data) {
            fillWireFrame(data.output);
            updatePaginateButtons(parseInt(data.next_page), parseInt(data.previous_page));
            $('#last-page').attr('data-page-number', parseInt(data.num_of_pages));
            $('.page-link').each(function (index, el) {
                $(el).attr('data-file-path', data.file_path);
            })
        },
        error: function (data) {
            let response = data.responseJSON;
            validation(response);
        }
    })
})

// paginat pages
$(document).on('click', '.page-link', function () {
    let page = $(this).attr('data-page-number');
    let max = $('#last-page').attr('data-page-number');
    // console.log(max);
    if (page != -1 && max >= page) {
        let path = $(this).data('file-path');
        $.ajax({
            type: 'GET',
            url: '/paginate-file?page=' + page + '&path=' + path,
            success: function (data) {
                fillWireFrame(data.output, parseInt(data.current_page));
                updatePaginateButtons(parseInt(data.next_page), parseInt(data.previous_page));
            }
        })
    }
})

// update data of link to next and previous pages
function updatePaginateButtons(nextPage, previousPage) {
    $('#previous-page').attr('data-page-number', previousPage);
    $('#next-page').attr('data-page-number', nextPage);
}

// fill wireframe
function fillWireFrame(data, page = 0) {
    page = page * 10
    $('#wireframe').empty();
    $.each(data, function (el) {
        let row = `<p>${el + 1 + page} ${data[el]}</p>`;
        $('#wireframe').append(row);
    });
}

//validation inputs
function validation(response) {
    $.each(response, function (el) {
        $('#' + el + 'Validation').text('*' + response[el])
    })
}