<?php


error_reporting(E_ALL);

ini_set('display_errors', 'On');

require __DIR__ . '/../src/Support/helper.php';

require base_path() . 'vendor/autoload.php';

require base_path() . 'routes/web.php';

$env = Dotenv\Dotenv::createImmutable(base_path());

$env->load();

app()->run();