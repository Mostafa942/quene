<div class="container">
    <div class="row justify-content-center pt-5">
        <div class="col-6">
            <form action="/login" method="POST" id="loginForm">
                <div class="mb-3 row">
                    <label for="username" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="username" name="username" placeholder="username" value="admin">
                        <div class="col-8">
                            <span class="text-danger" id="usernameValidation"></span>
                        </div>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="password" value="admin">
                        <div class="col-8">
                            <span class="text-danger" id="passwordValidation"></span>
                        </div>
                    </div>
                </div>
                <div class="mb-3 row justify-content-center">
                    <div class="col-2 text-center">
                        <button type="submit" class="btn btn-dark">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>