<div class="container">
    <form action="/get-file" method="POST" id="search-form">
        <div class="row justify-content-center">
            <div class="col-7">
                <input type="text" class="form-control" name="search">
                <div class="col-8">
                    <span class="text-danger" id="searchValidation"></span>
                </div>
            </div>
            <div class="col-3 text-center">
                <button type="submit" class="btn btn-secondary pl-4 pr-4">View</button>
            </div>
        </div>
    </form>
    <div class="row justify-content-center h-100 pt-5">
        <div class="col-9 h-100 p-3">
            <div class="bg-light p-4 border border-dark" id="wireframe">
               
            </div>
        </div>
        <div class="col-1"></div>
    </div>
    <div class="row justify-content-center">
        <div class="col-3">
            <div>
                <ul class="pagination">
                    <li class="page-item">
                        <button type="button" class="page-link" id="first-page" href="#" data-page-number="0">
                            <i class="fa-solid fa-backward-fast"></i>
                        </button>
                    </li>
                    <li class="page-item">
                        <button type="button" class="page-link" id="previous-page" href="#">
                            <i class="fa-solid fa-angle-left"></i>
                        </button>
                    </li>
                    <li class="page-item">
                        <button type="button" class="page-link" id="next-page" href="#">
                            <i class="fa-solid fa-angle-right"></i>
                        </button>
                    </li>
                    <li class="page-item">
                        <button type="button" class="page-link" id="last-page" href="#">
                            <i class="fa-solid fa-forward-fast"></i>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>